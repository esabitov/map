'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Parcels', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Borough: {
        type: Sequelize.STRING
      },
      Block: {
        type: Sequelize.INTEGER
      },
      Lot: {
        type: Sequelize.INTEGER
      },
      CD: {
        type: Sequelize.INTEGER
      },
      CT2010: {
        type: Sequelize.STRING
      },
      CB2010: {
        type: Sequelize.STRING
      },
      SchoolDist: {
        type: Sequelize.STRING
      },
      Council: {
        type: Sequelize.INTEGER
      },
      ZipCode: {
        type: Sequelize.INTEGER
      },
      FireComp: {
        type: Sequelize.STRING
      },
      PolicePrct: {
        type: Sequelize.INTEGER
      },
      HealthArea: {
        type: Sequelize.INTEGER
      },
      SanitBoro: {
        type: Sequelize.STRING
      },
      SanitDistr: {
        type: Sequelize.STRING
      },
      SanitSub: {
        type: Sequelize.STRING
      },
      Address: {
        type: Sequelize.STRING
      },
      ZoneDist1: {
        type: Sequelize.STRING
      },
      ZoneDist2: {
        type: Sequelize.STRING
      },
      ZoneDist3: {
        type: Sequelize.STRING
      },
      ZoneDist4: {
        type: Sequelize.STRING
      },
      Overlay1: {
        type: Sequelize.STRING
      },
      Overlay2: {
        type: Sequelize.STRING
      },
      SPDist1: {
        type: Sequelize.STRING
      },
      SPDist2: {
        type: Sequelize.STRING
      },
      SPDist3: {
        type: Sequelize.STRING
      },
      LtdHeight: {
        type: Sequelize.STRING
      },
      SplitZone: {
        type: Sequelize.STRING
      },
      BldgClass: {
        type: Sequelize.STRING
      },
      LandUse: {
        type: Sequelize.STRING
      },
      Easements: {
        type: Sequelize.INTEGER
      },
      OwnerType: {
        type: Sequelize.STRING
      },
      OwnerName: {
        type: Sequelize.STRING
      },
      LotArea: {
        type: Sequelize.INTEGER
      },
      BldgArea: {
        type: Sequelize.INTEGER
      },
      ComArea: {
        type: Sequelize.INTEGER
      },
      ResArea: {
        type: Sequelize.INTEGER
      },
      OfficeArea: {
        type: Sequelize.INTEGER
      },
      RetailArea: {
        type: Sequelize.INTEGER
      },
      GarageArea: {
        type: Sequelize.INTEGER
      },
      StrgeArea: {
        type: Sequelize.INTEGER
      },
      FactryArea: {
        type: Sequelize.INTEGER
      },
      OtherArea: {
        type: Sequelize.INTEGER
      },
      AreaSource: {
        type: Sequelize.STRING
      },
      NumBldgs: {
        type: Sequelize.INTEGER
      },
      NumFloors: {
        type: Sequelize.FLOAT
      },
      UnitsRes: {
        type: Sequelize.INTEGER
      },
      UnitsTotal: {
        type: Sequelize.INTEGER
      },
      LotFront: {
        type: Sequelize.FLOAT
      },
      LotDepth: {
        type: Sequelize.FLOAT
      },
      BldgFront: {
        type: Sequelize.FLOAT
      },
      BldgDepth: {
        type: Sequelize.FLOAT
      },
      Ext: {
        type: Sequelize.STRING
      },
      ProxCode: {
        type: Sequelize.STRING
      },
      IrrLotCode: {
        type: Sequelize.STRING
      },
      LotType: {
        type: Sequelize.STRING
      },
      BsmtCode: {
        type: Sequelize.STRING
      },
      AssessLand: {
        type: Sequelize.FLOAT
      },
      AssessTot: {
        type: Sequelize.FLOAT
      },
      ExemptLand: {
        type: Sequelize.FLOAT
      },
      ExemptTot: {
        type: Sequelize.FLOAT
      },
      YearBuilt: {
        type: Sequelize.INTEGER
      },
      YearAlter1: {
        type: Sequelize.INTEGER
      },
      YearAlter2: {
        type: Sequelize.INTEGER
      },
      HistDist: {
        type: Sequelize.STRING
      },
      Landmark: {
        type: Sequelize.STRING
      },
      BuiltFAR: {
        type: Sequelize.FLOAT
      },
      ResidFAR: {
        type: Sequelize.FLOAT
      },
      CommFAR: {
        type: Sequelize.FLOAT
      },
      FacilFAR: {
        type: Sequelize.FLOAT
      },
      BoroCode: {
        type: Sequelize.INTEGER
      },
      BBL: {
        type: Sequelize.FLOAT
      },
      CondoNo: {
        type: Sequelize.INTEGER
      },
      Tract2010: {
        type: Sequelize.STRING
      },
      XCoord: {
        type: Sequelize.INTEGER
      },
      YCoord: {
        type: Sequelize.INTEGER
      },
      ZoneMap: {
        type: Sequelize.STRING
      },
      ZMCode: {
        type: Sequelize.STRING
      },
      Sanborn: {
        type: Sequelize.STRING
      },
      TaxMap: {
        type: Sequelize.STRING
      },
      EDesigNum: {
        type: Sequelize.STRING
      },
      APPBBL: {
        type: Sequelize.FLOAT
      },
      APPDate: {
        type: Sequelize.STRING
      },
      PLUTOMapID: {
        type: Sequelize.STRING
      },
      Version: {
        type: Sequelize.STRING
      },
      MAPPLUTO_F: {
        type: Sequelize.INTEGER
      },
      SHAPE_Leng: {
        type: Sequelize.FLOAT
      },
      SHAPE_Area: {
        type: Sequelize.FLOAT
      },
      Shape: {
        type: Sequelize.GEOMETRY
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Parcels');
  }
};
