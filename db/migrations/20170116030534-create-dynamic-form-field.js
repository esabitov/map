'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('DynamicFormFields', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      type: {
        type: Sequelize.STRING
      },
      form_id: {
        type: Sequelize.INTEGER
      },
      required: {
        type: Sequelize.BOOLEAN
      },
      value: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      dynamic_form_id: {
        type: Sequelize.INTEGER,

        references: {
          model: 'DynamicForms',
          key: 'id',
          deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }

      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('DynamicFormFields');
  }
};
