const config = require('../config');

let sequelizeConfig = {
  url: config.databaseUrl, 
  dialect: 'postgres'
};

sequelizeConfig.production = sequelizeConfig;
sequelizeConfig.test = sequelizeConfig;
sequelizeConfig.development = sequelizeConfig;

//Exports final database config
module.exports = sequelizeConfig;