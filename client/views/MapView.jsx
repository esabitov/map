import React, {Component} from 'react'
import config from '../../config'
import { Row, Col, Button } from 'antd'
import { Link } from 'react-router'
import $ from 'jquery'
import MapSelectedView from '../components/MapSelectedView'
var mapStyles={retro:[{elementType:"geometry",stylers:[{color:"#ebe3cd"}]},{elementType:"labels.text.fill",stylers:[{color:"#523735"}]},{elementType:"labels.text.stroke",stylers:[{color:"#f5f1e6"}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#c9b2a6"}]},{featureType:"administrative.land_parcel",elementType:"geometry.stroke",stylers:[{color:"#dcd2be"}]},{featureType:"administrative.land_parcel",elementType:"labels.text.fill",stylers:[{color:"#ae9e90"}]},{featureType:"landscape.natural",elementType:"geometry",stylers:[{color:"#dfd2ae"}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#dfd2ae"}]},{featureType:"poi",elementType:"labels.text.fill",stylers:[{color:"#93817c"}]},{featureType:"poi.park",elementType:"geometry.fill",stylers:[{color:"#a5b076"}]},{featureType:"poi.park",elementType:"labels.text.fill",stylers:[{color:"#447530"}]},{featureType:"road",elementType:"geometry",stylers:[{color:"#f5f1e6"}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#fdfcf8"}]},{featureType:"road.highway",elementType:"geometry",stylers:[{color:"#f8c967"}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#e9bc62"}]},{featureType:"road.highway.controlled_access",elementType:"geometry",stylers:[{color:"#e98d58"}]},{featureType:"road.highway.controlled_access",elementType:"geometry.stroke",stylers:[{color:"#db8555"}]},{featureType:"road.local",elementType:"labels.text.fill",stylers:[{color:"#806b63"}]},{featureType:"transit.line",elementType:"geometry",stylers:[{color:"#dfd2ae"}]},{featureType:"transit.line",elementType:"labels.text.fill",stylers:[{color:"#8f7d77"}]},{featureType:"transit.line",elementType:"labels.text.stroke",stylers:[{color:"#ebe3cd"}]},{featureType:"transit.station",elementType:"geometry",stylers:[{color:"#dfd2ae"}]},{featureType:"water",elementType:"geometry.fill",stylers:[{color:"#b9d3c2"}]},{featureType:"water",elementType:"labels.text.fill",stylers:[{color:"#92998d"}]}]};

export default class MapView extends Component {

  constructor(props){
    super(props)
    // this.initMap = this.initMap.bind(this)
    // this.createMarkers = this.createMarkers.bind(this)
    // this.mappableSites = this.mappableSites.bind(this)
    // this.buildCleanSite = this.buildCleanSite.bind(this)
    this.state = {
      // markers: [],
      // selectedSite: {doThis: 'select a site'}
      parcels: []
    }
  }

  // componentDidMount () {
  //   window.initMap = this.initMap
  //   window.initMap()
  // }

  // componentDidUpdate () {
  //   console.log(this.mappableSites())
  //   console.log(this.state.markers.length)
  //   if (this.mappableSites().length !== this.state.markers.length){
  //     console.log('here')
  //     if (window.map){ this.setState ({ markers: this.createMarkers() })}
  //   }
  // }

  // mappableSites () {
  //   return this.props.sites.filter((site) => {
  //     return (site.lat.value !== null || site.lng.value !== null)
  //   })
  // }

  // initMap () {
  //   window.map = new google.maps.Map(this.refs.map, {zoom: 10, styles: mapStyles['retro']});
  //   var ctaLayer = new google.maps.KmlLayer({
  //     url: `https://blog.tomyancey.me/maybework.kml?dummy=${(new Date()).getTime()}`,
  //     map: map
  //   });
  // }

  // createMarkers () {
  //   var counter = 0
  //   return this.mappableSites().map((site, i) => {
  //     var {lat, lng} = site
  //     lat = lat.value; lng = lng.value
  //     if (i === 0) {
  //       window.map.setCenter({lat: Number(lat), lng: Number(lng)})
  //     }
  //     var infowindow = new google.maps.InfoWindow({
  //       content: `this is string ${counter}`
  //     })
  //     counter++
  //     var marker = new google.maps.Marker({
  //       position: {lat: Number(lat), lng: Number(lng)},
  //       map: window.map,
  //     })

  //     marker.addListener('click',() => {
  //       console.log(site)
  //       this.setState({selectedSite: this.buildCleanSite(site)})
  //     })
  //     return marker
  //   })
  // }

  // buildCleanSite(site){
  //   var cleanSite = {}
  //   Object.keys(site).forEach((key) => {
  //     cleanSite[key] = site[key]['editable'] === undefined ? site[key] : site[key]['value']
  //   })
  //   return cleanSite
  // }

  // componentWillMount() {
  //   const script = document.createElement("script");
  //   script.src = 'https://unpkg.com/leaflet@1.0.3/dist/leaflet.js';
  //   script.async = true;
  //   document.body.appendChild(script);

  //   const link = document.createElement("link");
  //   link.href = 'https://unpkg.com/leaflet@1.0.3/dist/leaflet.css';
  //   link.rel = 'stylesheet';
  //   document.body.appendChild(link);
  // }

  componentDidMount(){
    this.getParcels();    
  }

  displayMap(){
    var map = L.map('map').setView([37.8, -96], 4);
    L.tileLayer('https://api.tiles.mapbox.com/v4/mapbox.light/{z}/{x}/{y}.png?access_token=sk.eyJ1IjoiZWxsaW90c2FiaXRvdiIsImEiOiJjaXl3ZDdlM2UwMDFrMzhycTB6NDFmNWlhIn0.K8bgvoqgo5STcexHlaKwxg', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="http://mapbox.com">Mapbox</a>',
      id: 'mapbox.light'
    }).addTo(map);
    // control that shows state info on hover
    var info = L.control();
    info.onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info');
      this.update();
      return this._div;
    };
    info.update = function (props) {
      this._div.innerHTML = '<h4>US Population Density</h4>' +  (props ?
        '<b>' + props.name + '</b><br />' + props.density + ' people / mi<sup>2</sup>'
        : 'Hover over a state');
    };
    info.addTo(map);
    // get color depending on population density value
    function getColor(d) {
      return d > 1000 ? '#800026' :
          d > 500  ? '#BD0026' :
          d > 200  ? '#E31A1C' :
          d > 100  ? '#FC4E2A' :
          d > 50   ? '#FD8D3C' :
          d > 20   ? '#FEB24C' :
          d > 10   ? '#FED976' :
                '#FFEDA0';
    }
    function style(feature) {
      return {
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7,
        fillColor: getColor(feature.properties.density)
      };
    }
    function highlightFeature(e) {
      var layer = e.target;

      layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
      });

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      }

      info.update(layer.feature.properties);
    }
    var geojson;
    function resetHighlight(e) {
      geojson.resetStyle(e.target);
      info.update();
    }
    function zoomToFeature(e) {
      map.fitBounds(e.target.getBounds());
    }
    function onEachFeature(feature, layer) {
      layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
      });
    }
    debugger;
    geojson = L.geoJson(statesData, {
      style: style,
      onEachFeature: onEachFeature
    }).addTo(map);

    map.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">US Census Bureau</a>');
    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {
      var div = L.DomUtil.create('div', 'info legend'),
        grades = [0, 10, 20, 50, 100, 200, 500, 1000],
        labels = [],
        from, to;
      for (var i = 0; i < grades.length; i++) {
        from = grades[i];
        to = grades[i + 1];
        labels.push(
          '<i style="background:' + getColor(from + 1) + '"></i> ' +
          from + (to ? '&ndash;' + to : '+'));
      }
      div.innerHTML = labels.join('<br>');
      return div;
    };
    legend.addTo(map);
  }

  getParcels(){
    $.ajax({
      url: `${config.apiUrl}/parcels`,
      method: 'GET',
      headers: { 'x-access-token': localStorage.getItem('mappingJwtToken') },
      success: (data) => {
        console.log(data);
        this.setState({ parcels: data }, ()=>{
          this.displayMap();
        })
      },
      error: (error) => {
        console.log(error)
      }
    })
  }


  render (){
    return (
      <div>
        <div className="map" id="map">
          GO FUCKURSELF BEEACH
          {
          /*
          <Row>
            <Col xs={8} style={{'border': '1px solid #e9e9e9', 'height': '500px'}}>
              <MapSelectedView site={this.state.selectedSite}/>
            </Col>
            <Col xs={16}>
              <div id ='map' ref='map' style={{'border': '1px solid #e9e9e9', 'height': '500px'}}>I should be a map</div>
            </Col>
          </Row>
          */
          }
        </div>
      </div>
    )
  }
}
