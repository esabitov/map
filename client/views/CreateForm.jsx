import React, { Component } from 'react'
import DynamicFormFieldSet from '../components/DynamicFieldSet'

export default class FormIndex extends Component {
  render () {
    return (
      <div>
        <DynamicFormFieldSet />
      </div>
    )
  }
}
