import React from 'react'
import config from '../../config'
import { Form, Input, Icon, Checkbox, Button, Row, Col } from 'antd'
const FormItem = Form.Item

const Login = Form.create()(React.createClass({

  handleSubmit (e) {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.handleLogin(e, values)
      }
    })
  },

  loginOrSignUpPath () {
    return this.props.signup ? `${config.apiUrl}/users` : `${config.apiUrl}/auth/login`
  },

  render () {
    const { getFieldDecorator } = this.props.form

    return (
      <Row type="flex" justify="center">
        <Col xs={24} sm={12} md={6}>
          <Form action={this.props.signup ? `${config.apiUrl}/users` : `${config.apiUrl}/auth/login`} onSubmit={this.handleSubmit} className='login-form'>
            <FormItem>
              {getFieldDecorator('email', {
                rules: [{ required: true, message: 'Please input your email!' }]
              })(
                <Input addonBefore={<Icon type='user' />} placeholder='email' />
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }]
              })(
                <Input addonBefore={<Icon type='lock' />} type='password' placeholder='Password' />
              )}
            </FormItem>
            {
              this.props.signup
                ? <FormItem>
                  {getFieldDecorator('admin',
                    { valuePropName: 'checked', initialValue: false })(<Checkbox>Admin</Checkbox>)}
                  <Button type='primary' htmlType='submit' className='login-form-button'>
                    Sign up
                  </Button>
                  {' '}Or <a onClick={this.props.toggleSignup}>Log In</a>
                </FormItem>
                : <FormItem>
                  <Button type='primary' htmlType='submit' className='login-form-button'>
                    Login
                  </Button>
                  {' '}Or <a onClick={this.props.toggleSignup}>Sign Up</a>
                </FormItem>
            }
          </Form>
        </Col>
      </Row>
    )
  }
}))

module.exports = Login
