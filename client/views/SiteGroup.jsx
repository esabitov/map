import React, {Component} from 'react'
import config from '../../config'
import SitesTable from '../components/SitesTable'
import { Button, Card, Upload, Icon, message, Col, Spin } from 'antd'
import { Link } from 'react-router'
import $ from 'jquery'
const Dragger = Upload.Dragger

export default class SiteGroup extends Component {
  constructor (props) {
    super(props)
    this.state = {
      siteGroup: {},
      sites: [],
      uploading: false
    }
    this.handleAdd = this.handleAdd.bind(this)
    this.saveSite = this.saveSite.bind(this)
    this.createSite = this.createSite.bind(this)
    this.updateSite = this.updateSite.bind(this)
    this.deleteSite = this.deleteSite.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.updateStateWithSite = this.updateStateWithSite.bind(this)
    this.removeSiteFromState = this.removeSiteFromState.bind(this)
    this.getAllSites = this.getAllSites.bind(this)
    this.cellFieldBuilder = this.cellFieldBuilder.bind(this)
    this.handleCsvDownload = this.handleCsvDownload.bind(this)
    this.handleGeocode = this.handleGeocode.bind(this)
  }

  componentWillMount () {
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/sitegroups/${this.props.params.siteGroupid}`,
      method: 'get',
      headers: {'x-access-token': token},
      success: (siteGroup) => {
        this.setState({siteGroup})
      },
      error: (err) => console.log(err)
    })
    this.getAllSites()
  }

  getAllSites () {
    $.ajax({
      url: `${config.apiUrl}/sitegroups/${this.props.params.siteGroupid}/sites`,
      method: 'GET',
      headers: { 'x-access-token': localStorage.getItem('mappingJwtToken') },
      success: (sites) => {
        let data = sites.map((site,index) => {
          return this.cellFieldBuilder(site, index)
        })
        this.setState({ sites: data })
      },
      error: (error) => {
        console.log(error)
      }
    })
  }

  handleAdd () {
    if (!document.querySelector('#street_number-0')) {
      const sites = this.state.sites
      let count = sites.length
      let emptySite = { id: 0, street_number: '', street: '', unit: '', city: '', state: '', zip: '', extended_zip: '', parcel: '', lat: '', lng: '' }
      let newSite = this.cellFieldBuilder(emptySite, count, true)
      this.setState({
        sites: [...sites, newSite]
      })
    } else {
      alert('Save previous site before creating a new one!')
    }
  }

  cellFieldBuilder(site, index, editable=false){
    return {
      key: index,
      siteId: site.id,
      street_number: {
        editable: editable,
        value: site.street_number
      },
      street: {
        editable: editable,
        value: site.street
      },
      unit: {
        editable: editable,
        value: site.unit
      },
      city: {
        editable: editable,
        value: site.city
      },
      state: {
        editable: editable,
        value: site.state
      },
      zip: {
        editable: editable,
        value: site.zip
      },
      extended_zip: {
        editable: editable,
        value: site.extended_zip
      },
      parcel: {
        editable: editable,
        value: site.parcel
      },
      lat: {
        editable: editable,
        value: site.lat
      },
      lng: {
        editable: editable,
        value: site.lng
      }
    }
  }

  handleCsvDownload () {
    $('#download').submit()
  }

  updateStateWithSite (newSite) {
    let sites = this.state.sites
    sites.forEach((site) => {
      if (site.siteId === 0) {
        Object.keys(newSite).forEach((key) => {
          if (typeof site[key] !== 'undefined') {
            site[key] = {
              editable: false,
              value: newSite[key]
            }
          }
        })
      }
    })
    this.setState({sites})
  }

  createSite(data){
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/sitegroups/${this.props.params.siteGroupid}/sites`,
      method: 'post',
      data: data,
      headers: { 'x-access-token': token },
      success: (site) => {
        //update the state with the saved values
        this.updateStateWithSite(site);
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  updateSite(siteId,data){
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/sitegroups/${this.props.params.siteGroupid}/sites/${siteId}`,
      method: 'put',
      data: data,
      headers: { 'x-access-token': token },
      success: (site) => {
        //update the state with the saved values
        this.updateStateWithSite(site);
      },
      error: (error) => {
        console.log(error)
      }
    })
  }

  saveSite(siteId){
    let data = {
      street_number:document.querySelector('#street_number-'+siteId).value,
      street:document.querySelector('#street-'+siteId).value,
      unit:document.querySelector('#unit-'+siteId).value,
      city:document.querySelector('#city-'+siteId).value,
      state:document.querySelector('#state-'+siteId).value,
      zip:document.querySelector('#zip-'+siteId).value,
      extended_zip:document.querySelector('#extended_zip-'+siteId).value,
      parcel:document.querySelector('#parcel-'+siteId).value,
      lat:document.querySelector('#lat-'+siteId).value,
      lng:document.querySelector('#lng-'+siteId).value
    };
    let siteParams = {}
    Object.keys(data).forEach((key)=>{
      if(data[key] !== ''){
        siteParams[key] = data[key]
      }
    })
    if(siteId === 0){ //new site being created
      this.createSite(siteParams);
    }else{  //existing site upadte
      this.updateSite(siteId,siteParams);
    }
  }

  deleteSite(siteId){
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/sitegroups/${this.props.params.siteGroupid}/sites/${siteId}`,
      method: 'delete',
      headers: { 'x-access-token': token },
      success: (site) => {

      },
      error: (error) => {
        console.log(error)
      }
    })
  }

  handleDelete(siteId){
    this.removeSiteFromState(siteId)
    this.deleteSite(siteId)
  }

  handleGeocode(siteId){
    debugger
    console.log(this)
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/sitegroups/${this.props.params.siteGroupid}/sites/${siteId}/geocode`,
      method: 'post',
      datatype: 'json',
      headers: { 'x-access-token': token },
      success: (response) => {
        debugger
      },
      error: (err) => [
        console.log(err)
      ]
    })
  }

  removeSiteFromState(siteId){
    let sitesMinusSite = this.state.sites.filter((site)=>{
      return site.siteId !== siteId
    })
    this.setState({
      sites:sitesMinusSite
    })
  }

  render () {
    return (
      <div>
        <Col xs={10}>
          <Link to={`/sitegroups/${this.props.params.siteGroupid}/mapview`}>MapView</Link>
          <p><strong>created at:</strong> {this.state.siteGroup.createdAt}</p>
        </Col>

        <Col xs={4}>
          { this.state.uploading ? <Spin tip={'Loading'} size={'large'}></Spin> : '' }
        </Col>

          <Col xs={10}>
            <Upload
              name={'file'}
              multiple={true}
              showUploadList={false}
              action={`${config.apiUrl}/sitegroups/${this.props.params.siteGroupid}/upload`}
              data={{body: 'ya'}}
              headers={ {'x-access-token': localStorage.getItem('mappingJwtToken')} }
              onChange={(info) => {
                this.setState({ uploading: true })
                if (info.file.status === 'done') {
                  message.success(`${info.file.name} file uploaded successfully`)
                  this.getAllSites()
                  this.setState({ uploading: false })
                } else if (info.file.status === 'error') {
                  message.error(`${info.file.name} file upload failed.`)
                  this.setState({ uploading: false })
                }
              }}
            >
              <Button type="ghost">
                <Icon type="upload" /> Click to Upload Sites CSV
              </Button>
            </Upload>
          <form id='download' method='POST' action={`http://localhost:3000/sitegroups/${this.props.params.siteGroupid}/download`}>
            <input type='hidden' name='token' value={localStorage.getItem('mappingJwtToken')}/>
            <Button type="ghost" onClick={this.handleCsvDownload}>
              <Icon type="download" /> Click to Download Sites
            </Button>
          </form>
        </Col>
        {this.props.children ? React.cloneElement(this.props.children, Object.assign({}, this.state, {handleLogin: this.handleLogin, toggleSignup: this.toggleSignup, getToken: this.getToken})) : '' }
        <br />
        <div className='site-table'>
          <SitesTable
            sites={this.state.sites}
            handleAdd={this.handleAdd}
            handleGeocode={this.handleGeocode}
            saveSite={this.saveSite}
            handleDelete={this.handleDelete}
            removeSiteFromState={this.removeSiteFromState}
          />
        </div>
      </div>
    )
  }
}
