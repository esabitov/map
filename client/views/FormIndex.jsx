import React, { Component } from 'react'
import { Row, Col, Form} from 'antd'
import $ from 'jquery'
import config from '../../config'
import DynamicFormFieldSet from '../components/DynamicFieldSet'
import Forms from '../components/Forms'
import { Link } from 'react-router'

export default class FormIndex extends Component {
  constructor (props) {
    super(props)
    this.state = {
      forms: []
    }
    this.getForms = this.getForms.bind(this)
  }

  componentWillMount () {
    this.getForms()
  }

  getForms () {
    $.ajax({
      url: `${config.apiUrl}/forms`,
      method: 'get',
      headers: { 'x-access-token': localStorage.getItem('mappingJwtToken') },
      success: ((forms) => this.setState({ forms })),
      error: ((err) => console.log(err) )
    })
  }

  render () {
    return (
      <div>
        <h1>Forms</h1>
        <Link to='/forms/new'>Add New Form</Link>
        <Forms forms={this.state.forms}/>
      </div>
    )
  }
}
