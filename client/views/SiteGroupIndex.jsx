import React, {Component} from 'react'
import config from '../../config'
import $ from 'jquery'
import SiteGroups from '../components/SiteGroups'
import CreateSiteGroup from '../components/CreateSiteGroup'
import { notification } from 'antd'

const openNotificationWithIcon = (type, message, description) => {
  notification[type]({
    message,
    description
  })
}

export default class SiteGroupIndex extends Component {

  constructor (props) {
    super(props)
    this.state = {
      siteGroups: []
    }
    this.addNewSiteGroup = this.addNewSiteGroup.bind(this)
  }

  addNewSiteGroup (data) {
    let siteGroupParams = Object.assign({}, data, {token: localStorage.getItem('mappingJwtToken')})
    $.ajax({
      url: `${config.apiUrl}/sitegroups`,
      method: 'post',
      data: siteGroupParams,
      success: (newSitegroup) => {
        if (newSitegroup.success === true) {
          let siteGroups = [...this.state.siteGroups, newSitegroup]
          this.setState({siteGroups})
        } else {
          openNotificationWithIcon('info', 'Site Group not created', 'must be an organization admin to create a site')
        }
      },
      error: (err) => {
        console.log(err)
      }
    })
  }

  componentWillMount () {
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/sitegroups`,
      method: 'GET',
      headers: { 'x-access-token': token },
      success: (siteGroups) => {
        this.setState({ siteGroups })
      },
      error: (error) => {
        console.log(error)
      }
    })
  }

  render () {
    return (
      <div className='SiteGroupIndex'>
        <CreateSiteGroup getToken={this.props.getToken} addNewSiteGroup={this.addNewSiteGroup} />
        <SiteGroups siteGroups={this.state.siteGroups} />
      </div>
    )
  }
}
