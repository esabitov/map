import React from 'react'
import { Navigation, hashHistory, Link } from 'react-router'
import Nav from '../components/Nav'
import config from '../../config'
import $ from 'jquery'
import { Layout, Menu, notification } from 'antd'
const { Header, Content, Footer } = Layout

const openNotificationWithIcon = (type, message, description) => {
  notification[type]({
    message,
    description
  })
}

export default class App extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      signup: false,
      loggedIn: false
    }
    this.handleLogin = this.handleLogin.bind(this)
    this.setToken = this.setToken.bind(this)
    this.toggleSignup = this.toggleSignup.bind(this)
    this.getToken = this.getToken.bind(this)
    this.setToken = this.setToken.bind(this)
    this.logOut = this.logOut.bind(this)
    this.loggedIn = this.loggedIn.bind(this)
    this.checkAuth = this.checkAuth.bind(this)
  }

  componentDidMount () {
    this.checkAuth()
  }

  setToken (token) {
    localStorage.setItem('mappingJwtToken', token)
    this.setState({ loggedIn: true })
  }

  getToken () {
    const token = localStorage.getItem('mappingJwtToken')
    return token
  }

  logOut () {
    hashHistory.push('/login')
    localStorage.removeItem('mappingJwtToken')
    this.setState({ loggedIn: false })
  }

  loggedIn () {
    this.setState({
      loggedIn: !!this.getToken()
    })
  }

  handleLogin (e, userInfo) {
    $.ajax({
      context: this,
      url: e.target.action,
      data: userInfo,
      method: 'post',
      success: (res) => {
        if (res.success) {
          this.setToken(res.token)
          this.setState({ loggedIn: true })
        } else {
          openNotificationWithIcon('error', 'Authentication Failed', res.message)
        }
      },
      error: (err) => {
        openNotificationWithIcon('error', 'Authentication Failed', err.message)
      }
    })
  }

  checkAuth () {
    let loggedIn = false
    const token = this.getToken()
    if (token) {
      $.ajax({
        url: `${config.apiUrl}/auth/verify`,
        method: 'post',
        data: {token: token},
        success: (res) => {
          if (res.email) {
            this.setState({loggedIn: true})
          } else {
            hashHistory.push('/login')
          }
        },
        error: (err) => {
          console.log(err)
        }
      })
    } else {
      hashHistory.push('/login')
      this.setState({loggedIn: false})
    }
  }

  toggleSignup () {
    this.setState({ signup: !this.state.signup })
  }

  render () {
    return (
      <Layout>
        <Header id='header'>
          <Nav signup={this.state.signup}
            toggleSignup={this.toggleSignup}
            logOut={this.logOut}
            loggedIn={this.state.loggedIn}
            />
        </Header>
          <Content>
            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
              {this.props.children ? React.cloneElement(this.props.children, Object.assign({}, this.state, {handleLogin: this.handleLogin, toggleSignup: this.toggleSignup, getToken: this.getToken})) : '' }
            </div>
          </Content>
          <Footer>
            Map stuff
          </Footer>
      </Layout>
    )
  }
}
