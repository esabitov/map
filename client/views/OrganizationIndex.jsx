import React, { Component } from 'react'

import OrganizationsTable from '../components/OrganizationsTable'
import { notification } from 'antd'

// css
import $ from 'jquery'
import config from '../../config'

const openNotificationWithIcon = (type, message, description) => {
  notification[type]({
    message,
    description
  })
}

export default class OrganizationIndex extends Component {
  constructor (props) {
    super(props)
    this.state = {
      items:[],
    }
    this.handleAdd = this.handleAdd.bind(this)
    this.updateState = this.updateState.bind(this)
    this.create = this.create.bind(this)
    this.update = this.update.bind(this)
    this.save = this.save.bind(this)
    this.delete = this.delete.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.removeFromState = this.removeFromState.bind(this)    
  }

  componentWillMount () {
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/organizations`,
      method: 'GET',
      headers: { 'x-access-token': token },
      success: (items) => {
        let data = items.map((item,index)=>{
          return {
            key: index,
            id: item.id,
            name: {
              editable: false,
              value: item.name,
            }
          }
        });
        this.setState({
          items:data
        })
      },
      error: (error) => {
        console.log(error)
      }
    })
  }

  handleAdd(){
    if(!document.querySelector('#name-0')){
      const items = this.state.items;
      let count = items.length;
      const newItem = {
        key: count,
        id: 0,
        name: {
          editable: true,
          value: '',
        }
      };
      this.setState({
        items: [...items, newItem],
      });
    }else{
      alert('Save previous item before creating a new one!');
    }
  }

  updateState(newItem){
    let items = this.state.items;
    items.forEach((item)=>{
      if(item.id === 0){
        Object.keys(newItem).forEach((key)=>{
          if(typeof item[key] !== 'undefined'){
            item[key] = {
              editable: false,
              value: newItem[key]
            }
          }
        });
      }
    });
    this.setState({items})
  }

  create(data) {
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/organizations`,
      method: 'post',
      data: data,
      headers: { 'x-access-token': token },
      success: (item) => {
        //update the state with the saved values
        this.updateState(item);
        openNotificationWithIcon('success', 'Created successfully!', '')
      },
      error: (err) => {
        openNotificationWithIcon('error', '', err)
      }
    })
  }

  update(id,data){
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/organizations/${id}`,
      method: 'put',
      data: data,
      headers: { 'x-access-token': token },
      success: (item) => {
        //update the state with the saved values
        this.updateState(item);
        openNotificationWithIcon('success', 'Updated successfully!', '')
      },
      error: (err) => {
        openNotificationWithIcon('error', '', err)
      }
    })
  }

  save(id){
    let data = {
      name:document.querySelector('#name-'+id).value,
    };
    let params = {}
    Object.keys(data).forEach((key)=>{
      if(data[key] !== ''){
        params[key] = data[key]
      }
    })
    if(id === 0){ //new being created
      this.create(params);
    }else{  //existing  upadte
      this.update(id,params);
    }
  }

  delete(id){
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/organizations/${id}`,
      method: 'delete',
      headers: { 'x-access-token': token },
      success: (item) => {
        openNotificationWithIcon('success', 'Removed successfully!', '')
      },
      error: (err) => {
        openNotificationWithIcon('error', '', err)
      }
    })
  }

  handleDelete(id){
    this.removeFromState(id)
    this.delete(id)
  }

  removeFromState(id){
    let itemsMinusOne = this.state.items.filter((item)=>{
      return item.id !== id
    })
    this.setState({
      items:itemsMinusOne
    });
  }

  render() {
    return (
      <div className="organizations">
        <OrganizationsTable
          items={this.state.items}
          handleAdd={this.handleAdd}
          save={this.save}
          handleDelete={this.handleDelete}
          removeFromState={this.removeFromState}
        />
      </div>
    )
  }
}
