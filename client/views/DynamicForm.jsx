import React, { Component } from 'react'
import config from '../../config'
import $ from 'jquery'
import { notification } from 'antd'

const openNotificationWithIcon = (type, message, description) => {
  notification[type]({
    message,
    description
  })
}

export default class DynamicForm extends Component {

  constructor (props) {
    super(props)
    this.state = {
      form: {},
      formFields: []
    }
    this.getForm = this.getForm.bind(this)
    this.getFormFields = this.getFormFields.bind(this)
  }

  componentWillMount(){
    this.getForm()
    this.getFormFields()
  }

  getForm(){
    $.ajax({
      url: `${config.apiUrl}/forms/${this.props.params.id}`,
      headers: { 'x-access-token': localStorage.getItem('mappingJwtToken') },
      method: 'GET',
      success: ((form) => {
        (form.success !== false) ?
        this.setState({ form }) :
        openNotificationWithIcon('error', 'error', form.message)
      }),
      error: ((err) => openNotificationWithIcon('error', '', err))
    })
  }

  getFormFields () {
    $.ajax({
      url: `${config.apiUrl}/forms/${this.props.params.id}/formFields`,
      headers: { 'x-access-token': localStorage.getItem('mappingJwtToken') },
      method: 'GET',
      success: ((formFields) => {
        this.setState({ formFields })
      }),
      error: ((err) => openNotificationWithIcon('error', '', err))
    })
  }

  render () {
    return (
      <div>
        <h1>{ this.state.form.title }</h1>
        { JSON.stringify(this.state.formFields, null, 4) }
      </div>
    )
  }
}
