import React, { Component } from 'react'
import { Row, Col } from 'antd'
import SiteGroupIndex from './SiteGroupIndex'
import FormIndex from './FormIndex'

export default class Dashboard extends Component {
  render () {
    return (
      <Row>
        <Col xs={12}>
          <SiteGroupIndex />
        </Col>
        <Col xs={12}>
          <FormIndex />
        </Col>
      </Row>
    )
  }
}
