import React, {Component} from 'react'
import { Table, Input, Icon, Button, Popconfirm } from 'antd';

class EditableCell extends Component {
  state = {
    value: this.props.value,
    editable: this.props.editable || false,
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.editable !== this.state.editable) {
      this.setState({ editable: nextProps.editable });
      if (nextProps.editable) {
        this.cacheValue = this.state.value;
      }
    }
    if (nextProps.status && nextProps.status !== this.props.status) {
      if (nextProps.status === 'save') {
        this.props.onChange(this.state.value);
      } else if (nextProps.status === 'cancel') {
        this.setState({ value: this.cacheValue });
        this.props.onChange(this.cacheValue);
      }
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.editable !== this.state.editable ||
           nextState.value !== this.state.value;
  }
  handleChange(e) {
    const value = e.target.value;
    this.setState({ value });
  }
  render() {
    const { value, editable } = this.state;
    return (<div>
      {
        editable ?
        <div>
          <Input
            value={value}
            onChange={e => this.handleChange(e)}
            id={this.props.id}
          />
        </div>
        :
        <div className="editable-row-text">
          {value.toString() || ' '}
        </div>
      }
    </div>);
  }
}

export default class SitesTable extends Component {
  constructor(props) {
    super(props);
    this.columns = [{
      title: 'street_number',
      dataIndex: 'street_number',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'street_number', text),
    }, {
      title: 'street',
      dataIndex: 'street',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'street', text),
    }, {
      title: 'unit',
      dataIndex: 'unit',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'unit', text),
    }, {
      title: 'city',
      dataIndex: 'city',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'city', text),
    }, {
      title: 'state',
      dataIndex: 'state',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'state', text),
    }, {
      title: 'zip',
      dataIndex: 'zip',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'zip', text),
    }, {
      title: 'extended_zip',
      dataIndex: 'extended_zip',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'extended_zip', text),
    }, {
      title: 'parcel',
      dataIndex: 'parcel',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'parcel', text),
    }, {
      title: 'lat',
      dataIndex: 'lat',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'lat', text),
    }, {
      title: 'lng',
      dataIndex: 'lng',
      render: (text, record, index) => this.renderColumns(this.props.sites, index, 'lng', text),
    }, {
      title: 'operation',
      dataIndex: 'operation',
      fixed: 'right',
      width: 150,
      render: (text, record, index) => {
        const { editable } = this.props.sites[index].street_number;
        return (<div className="editable-row-operations">
          {
            editable ?
            <span>
              <a onClick={() => this.editDone(index, 'save')}>Save</a>
              <Popconfirm title="Sure to cancel?" onConfirm={() => this.editDone(index, 'cancel')}>
                <a>Cancel</a>
              </Popconfirm>
            </span>
            :
            <span>
              <a onClick={() => this.edit(index)}>Edit</a>
            </span>
          }
          <span>
            <a onClick={() => this.delete(index)}>Delete</a>
          </span>
          <span>
            <a onClick={() => this.geocode(index)}>geocode</a>
          </span>
        </div>);
      },
    }];
  }
  renderColumns(data, index, key, text) {
    const { editable, status } = data[index][key];
    if (typeof editable === 'undefined') {
      return text;
    }
    return (<EditableCell
      editable={editable}
      value={text ? text : ''}
      onChange={value => this.handleChange(key, index, value)}
      status={status}
      id={key+'-'+data[index].siteId}
    />);
  }
  handleChange(key, index, value) {
    const data = this.props.sites;
    data[index][key].value = value;
    this.setState({ data });
  }
  edit(index) {
    const data = this.props.sites;
    Object.keys(data[index]).forEach((item) => {
      if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
        data[index][item].editable = true;
      }
    });
    this.setState({ data });
  }
  editDone(index, type) {
    const data = this.props.sites;
    let siteId = data[index].siteId;
    if(type == 'save'){
      this.props.saveSite(siteId);
    }
    Object.keys(data[index]).forEach((item) => {
      if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
        data[index][item].editable = false;
        data[index][item].status = type;
      }
    });
    this.setState({ data });
  }
  delete(index){
    this.props.handleDelete(this.props.sites[index].siteId);
  }

  geocode (index) {
    this.props.handleGeocode(this.props.sites[index].siteId);
  }

  handleAdd() {
    this.props.handleAdd();
  }
  render() {
    const data = this.props.sites;
    const dataSource = data.map((item) => {
      const obj = {};
      Object.keys(item).forEach((key) => {
        obj[key] = key === 'key' ? item[key] : item[key] !== null ? item[key].value : '';
      });
      return obj;
    });
    const columns = this.columns;
    return (<div>
      <Button className="editable-add-btn" type="ghost" onClick={this.handleAdd.bind(this)}>Add</Button>
      <Table dataSource={dataSource} columns={columns} scroll={{ x: 1400 }} size='small' />
    </div>);
  }
}
