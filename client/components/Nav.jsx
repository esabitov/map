import React, { Component } from 'react'
import { Link } from 'react-router'
import { Menu } from 'antd'

export default class Nav extends Component {

  constructor (props) {
    super(props)
    this.alerter = this.alerter.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  alerter () {
    alert('ya')
  }

  handleClick (e) {
    console.log('click ', e)
    if (e.key === '4'){
      this.props.logOut()
    }
    if (e.key === '5'){
      this.props.toggleSignup()
    }
    this.setState({
      current: e.key
    })
  }

  render () {
    return (
        <Menu
          theme="light"
          mode="horizontal"
          defaultSelectedKeys={['1']}
          style={{ lineHeight: '64px' }}
          onClick={this.handleClick}
        >
        { this.props.loggedIn ? <Menu.Item key="1"><Link to={'/dashboard'}>Dashboard</Link></Menu.Item> : '' }
        { this.props.loggedIn ? <Menu.Item key="2"><Link to={'/sitegroups'}>sitegroups</Link></Menu.Item> : '' }
        { this.props.loggedIn ? <Menu.Item key="3"><Link to={'/organizations'}>Organizations</Link></Menu.Item> : '' }
        { this.props.loggedIn ? <Menu.Item key="6"><Link to={'/forms'}>Forms</Link></Menu.Item> : '' }
        { this.props.loggedIn ? <Menu.Item key="4" onClick={this.alerter}>LogOut</Menu.Item> : '' }
        { !this.props.loggedIn ? <Menu.Item key="5" onClick={this.props.toggleSignup}>{this.props.signup ? 'Sign In' : 'Sign up'}</Menu.Item> : '' }
      </Menu>
    )
  }
}
