import React, { Component } from 'react'
import { Card } from 'antd'
import { Link } from 'react-router'

const FormCard = (props) => {
  return (
    <Link to={`forms/${props.id}`}>
      <Card title={props.title}
        bordered={false}
      >
        creator: {props.user_id}
      </Card>
    </Link>
  )
}

const Forms = (props) => {
  return (
    <div>
      { props.forms.map((form) => <FormCard {...form} />) }
    </div>
  )
}

export default Forms
