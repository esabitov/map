import React from 'react'
import { Select, Row, Col, Form, Input, Icon, Checkbox, Button, notification } from 'antd'
import { hashHistory } from 'react-router'
const Option = Select.Option
const FormItem = Form.Item
import $ from 'jquery'
import config from '../../config'

const openNotificationWithIcon = (type, message, description) => {
  notification[type]({
    message,
    description
  })
}

let formKeys = [0]
const DynamicFieldSet = Form.create()(React.createClass({
  createForm (values) {
    let formFields = this.extractFormFields(values)
    let formData = {
      formFields: formFields,
      formTitle: values.Form.title,
      token: localStorage.getItem('mappingJwtToken')
    }
    $.ajax({
      url: `${config.apiUrl}/forms`,
      method: 'post',
      data: formData,
      success: ((res) => {
        if (res.length === formFields.length){
          openNotificationWithIcon('success', 'Form Created', 'You have successfully created this form')
          hashHistory.push('/forms')
        }
      }),
      error: ((err) => console.log(err))
    })
  },

  extractFormFields (values) {
    return values.name.map((name, i) => {
      return Object.assign({},
        {
          name: values.name[i],
          type: values.type[i],
          value: values.value[i],
          required: values.required[i]
        }
      )
    })
  },

  handleSubmit (e) {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.createForm(values)
      }
    })
  },

  add () {
    formKeys[formKeys.length] = formKeys[formKeys.length - 1] + 1
    this.forceUpdate()
  },

  remove (k) {
    formKeys = formKeys.filter((key) => key !== k)
    this.forceUpdate()
  },

  renderFieldName (formKey, getFieldDecorator) {
    return (
      <FormItem key={formKey + 0.1}
        label='name'>
        {getFieldDecorator(`name[${formKey}]`, {
          rules: [{ required: true, message: 'Please input the field name' }]
        })(
          <Input type='text' placeholder='Field name' />
        )}
      </FormItem>
    )
  },

  renderFieldType (formKey, getFieldDecorator) {
    return (
      <FormItem key={formKey + 0.2}
        label='type'>
        {getFieldDecorator(`type[${formKey}]`, {
          rules: [
            { required: true, message: 'Please input the field type' }
          ]
        })(
          <Select placeholder='Please select a country'>
            <Option value='text'>text</Option>
            <Option value='checkbox'>checkbox</Option>
            <Option value='rating'>rating</Option>
          </Select>
        )}
      </FormItem>
    )
  },

  renderFieldDefaultValue (formKey, getFieldDecorator) {
    return (
      <FormItem key={formKey + 0.3}
        label='field value'>
        {getFieldDecorator(`value[${formKey}]`, {
          rules: [{ required: false }]
        })(
          <Input type='text' placeholder='Default Field Value' />
        )}
      </FormItem>
    )
  },

  renderFieldRequired (formKey, getFieldDecorator) {
    return (
      <FormItem key={formKey + 0.4}
        label='required'>
        {getFieldDecorator(`required[${formKey}]`, {
          valuePropName: 'checked',
          initialValue: true
        })(
          <Checkbox>Required Field</Checkbox>
        )}
      </FormItem>
    )
  },

  renderDeleteIcon (formKey) {
    return (
      <Icon
        className='dynamic-delete-button'
        type='minus-circle-o'
        onClick={() => this.remove(formKey)}
      />
    )
  },

  formItems (formKey) {
    const { getFieldDecorator } = this.props.form
    return (
      <Row type='flex' align='middle' justify='space-between'>
        <Col xs={24} sm={12} md={5}>
          { this.renderFieldName(formKey, getFieldDecorator) }
        </Col>
        <Col xs={24} sm={12} md={5}>
          { this.renderFieldType(formKey, getFieldDecorator) }
        </Col>
        <Col xs={24} sm={12} md={5}>
          { this.renderFieldDefaultValue(formKey, getFieldDecorator) }
        </Col>
        <Col xs={24} sm={12} md={5}>
          { this.renderFieldRequired(formKey, getFieldDecorator) }
        </Col>
        <Col xs={2}>
          {formKey === formKeys[formKeys.length - 1] ? this.renderDeleteIcon(formKey) : '' }
        </Col>
      </Row>
    )
  },

  render () {
    const { getFieldDecorator } = this.props.form
    const SubForms = formKeys.map((formKey) => this.formItems(formKey))
    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 16 }
    }
    return (
      <Form onSubmit={this.handleSubmit} className='create-form'>
        <FormItem
          {...formItemLayout}
          label='Form Title'
          >
          {getFieldDecorator('Form.title', {
            rules: [{required: true, message: 'please name this form'}]
          })(
            <Input type='text' placeholder='Form Title' />
          )}
        </FormItem>
        {SubForms}
        <Button type='primary' htmlType='submit' className='create-form-button'>
          Create Form
        </Button>
        <Button type='default' onClick={this.add}>Add Form Field</Button>
      </Form>
    )
  }
}))

module.exports = DynamicFieldSet
