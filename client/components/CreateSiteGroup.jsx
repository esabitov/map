import React from 'react'
import { Form, Input, Button, notification } from 'antd'
const FormItem = Form.Item

const CreateSiteGroup = Form.create()(React.createClass({
  handleSubmit (e) {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values)
        this.props.addNewSiteGroup(values)
      }
    })
  },

  render () {
    const { getFieldDecorator } = this.props.form

    return (
      <Form onSubmit={this.handleSubmit} className='login-form'>
        <FormItem>
          {getFieldDecorator('title', {
            rules: [{ required: true, message: 'Site Group cannot be blank' }]
          })(
            <Input placeholder='site group' />
          )}
        </FormItem>
        <FormItem>
          <Button type='primary' htmlType='submit' className='create-site-group-submit-button'>
            Create New Site Group
          </Button>
        </FormItem>
      </Form>
    )
  }
}))

module.exports = CreateSiteGroup
