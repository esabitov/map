import React, { Component } from 'react'
import { Link } from 'react-router'
import { Card, Col } from 'antd'

export default class SiteGroupLink extends Component {
  render () {
    return (
      <Col className='gutter-row' xs={12} sm={4}>
        <div className='gutter-box'>
          <Link to={`sitegroups/${this.props.id}`}><Card title={this.props.title}></Card></Link>
        </div>
      </Col>
    )
  }
}
