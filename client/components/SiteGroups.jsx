import React from 'react'
import SiteGroupLink from './SiteGroupLink'
import { Row } from 'antd'

export default class SiteGroups extends React.Component {
  constructor (props) {
    super(props)
    this.renderSiteGroups = this.renderSiteGroups.bind(this)
  }

  renderSiteGroups () {
    return this.props.siteGroups.map((siteGroup, index) => {
      return <SiteGroupLink key={index} {...siteGroup} />
    })
  }

  render () {
    return (
      <Row>
        { this.renderSiteGroups() }
      </Row>
    )
  }

}
