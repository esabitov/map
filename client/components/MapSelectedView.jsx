import React, {Component} from 'react'
import { Col, Row } from 'antd'
import $ from 'jquery'

export default class MapSelectedView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      imageUrl: '#',
      currentAddress: '',
      currentLatLng: ''
    }
    this.attemptToPlaceImage = this.attemptToPlaceImage.bind(this)
    this.checkForImage = this.checkForImage.bind(this)
    this.retrieveAndPlaceImage = this.retrieveAndPlaceImage.bind(this)
    this.latLngBuilder = this.latLngBuilder.bind(this)
    this.addressBuilder = this.addressBuilder.bind(this)
    this.urlBuilder = this.urlBuilder.bind(this)
    this.unlessNull = this.unlessNull.bind(this)
    // this.unlessNull = this.unlessNull.bind(this)
  }

  componentDidUpdate () {
    var firstState = this.props.site.doThis !== undefined
    var duplicateAddressCall = this.state.currentAddress === this.addressBuilder()
    var duplicateLatLngCall = this.state.currentLatLng === this.latLngBuilder()
    if (!(firstState || (duplicateLatLngCall && duplicateAddressCall))) {
      this.attemptToPlaceImage()
    }
  }

  attemptToPlaceImage () {
    var latLngCheck = this.checkForImage()
    var addressCheck = this.checkForImage(true)
    var currentAddress = this.addressBuilder()
    var currentLatLng = this.latLngBuilder()
    var that = this

    addressCheck.then((response) => {
      if (response.status === 'OK'){
        this.setState({ imageUrl: that.urlBuilder(currentAddress, false), currentAddress, currentLatLng })
      } else {
        latLngCheck.then((response) => {
          if (response.status === 'OK'){
            this.setState({ imageUrl: that.urlBuilder(currentLatLng, false), currentAddress, currentLatLng })
          } else {
            this.setState({ imageUrl: '#', currentAddress, currentLatLng })
          }
        })
      }
    }).catch((err) => { this.setState({ imageUrl: '#', currentAddress, currentLatLng }) })
  }

  retrieveAndPlaceImage (data) {
    console.log('yo')
  }

  latLngBuilder () {
    var { lat, lng } = this.props.site
    return `${lat},${lng}`
  }

  unlessNull (thing) {
    var returnThing = thing ? thing : ''
    return returnThing
  }

  addressBuilder () {
    let unlessNull = this.unlessNull
    var { street_number, street, unit, city, state, zip } = this.props.site
    var str = `${unlessNull(street_number)} ${unlessNull(street)} ${unlessNull(unit)} ${unlessNull(city)} ${unlessNull(state)} ${unlessNull(zip)}`
    var newstr = str.replace(/\s\s+/g, ' ').replace(/ /g,'+').slice(0, -1)
    return newstr
  }

  urlBuilder (location, meta=false) {
    return `https://maps.googleapis.com/maps/api/streetview${ meta ? '/metadata' : '' }?size=450x200&location=${location}&key=AIzaSyC0X-zC3v1l0sDQnTXnV8N6McaSzmGRo5I`
  }

  checkForImage (addressCheck=false) {
    return new Promise((resolve, reject) => {
      var query = addressCheck ? this.addressBuilder() : this.latLngBuilder()
      var url = this.urlBuilder(query, true)
      $.ajax({
        url,
        dataType: 'json',
        method: 'get',
        success: ((data) => {
          resolve(data)
        }),
        error: ((err) => {
          reject(err)
        })
      })
    })
  }

  checkForImageByAddress () {
    return new Promise((resolve, reject) => {
      var url = this.urlBuilder(this.addressBuilder(), true)
      $.get( url, (data, status) => {
      })
    })
  }

  render () {
    return (
      <div id='map-selected-view'>
        <Col xs={24}>
          <img src={this.state.imageUrl}/>
          {JSON.stringify(this.props.site, null, 4)}
        </Col>
      </div>
    )
  }
}
