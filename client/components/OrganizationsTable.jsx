import React, {Component} from 'react'
import { Table, Input, Icon, Button, Popconfirm, Form, notification } from 'antd';
const FormItem = Form.Item;

import $ from 'jquery'
import config from '../../config'

const openNotificationWithIcon = (type, message, description) => {
  notification[type]({
    message,
    description
  })
}

class EditableCell extends Component {
  state = {
    value: this.props.value,
    editable: this.props.editable || false,
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.editable !== this.state.editable) {
      this.setState({ editable: nextProps.editable });
      if (nextProps.editable) {
        this.cacheValue = this.state.value;
      }
    }
    if (nextProps.status && nextProps.status !== this.props.status) {
      if (nextProps.status === 'save') {
        this.props.onChange(this.state.value);
      } else if (nextProps.status === 'cancel') {
        this.setState({ value: this.cacheValue });
        this.props.onChange(this.cacheValue);
      }
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.editable !== this.state.editable ||
           nextState.value !== this.state.value;
  }
  handleChange(e) {
    const value = e.target.value;
    this.setState({ value });
  }
  render() {
    const { value, editable } = this.state;
    return (<div>
      {
        editable ?
        <div>
          <Input
            value={value}
            onChange={e => this.handleChange(e)}
            id={this.props.id}
          />
        </div>
        :
        <div className="editable-row-text">
          {value.toString() || ' '}
        </div>
      }
    </div>);
  }
}

export default class OrganizationsTable extends Component {
  constructor(props) {
    super(props);
    this.columns = [{
      title: 'Organization Name',
      dataIndex: 'name',
      render: (text, record, index) => this.renderColumns(this.props.items, index, 'name', text),
    }, {
      title: 'Action',
      dataIndex: 'operation',
      width:200,
      render: (text, record, index) => {
        const { editable } = this.props.items[index].name;
        return (<div className="editable-row-operations">
          {
            editable ?
            <span>
              <a onClick={() => this.editDone(index, 'save')}>Save</a>
              <Popconfirm title="Sure to cancel?" onConfirm={() => this.editDone(index, 'cancel')}>
                <a>Cancel</a>
              </Popconfirm>
            </span>
            :
            <span>
              <a onClick={() => this.edit(index)}>Edit</a>
            </span>
          }
          <span>
            <a onClick={() => this.delete(index)}>Delete</a>
            {this.props.items[index].id !== 0 && 
              <a onClick={() => this.initInvite(index)}>Invite</a>
            }
          </span>
        </div>);
      },
    }];
    this.state = {
      inviteOrgId:false,
      email:false,
      inviteActive:false
    }
  }
  renderColumns(data, index, key, text) {
    const { editable, status } = data[index][key];
    if (typeof editable === 'undefined') {
      return text;
    }
    return (<EditableCell
      editable={editable}
      value={text ? text : ''}
      onChange={value => this.handleChange(key, index, value)}
      status={status}
      id={key+'-'+data[index].id}
    />);
  }
  handleChange(key, index, value) {
    const data = this.props.items;
    data[index][key].value = value;
    this.setState({ data });
  }
  edit(index) {
    const data = this.props.items;
    Object.keys(data[index]).forEach((item) => {
      if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
        data[index][item].editable = true;
      }
    });
    this.setState({ data });
  }
  editDone(index, type) {
    const data = this.props.items;
    let id = data[index].id;
    if(type == 'save'){
      this.props.save(id);
    }
    if(type === 'cancel' && id === 0){
      this.props.removeFromState(id);
    }
    Object.keys(data[index]).forEach((item) => {
      if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
        data[index][item].editable = false;
        data[index][item].status = type;
      }
    });
    this.setState({ data });
  }
  delete(index){
    this.props.handleDelete(this.props.items[index].id);
  }
  handleAdd() {
    this.props.handleAdd();
  }
  initInvite(index){
    this.setState({
      inviteOrgId:this.props.items[index].id,
      inviteActive:true
    })
  }
  submitInvite(){
    let orgId = this.state.inviteOrgId;
    let email = this.state.email.value;
    let token = localStorage.getItem('mappingJwtToken')
    $.ajax({
      url: `${config.apiUrl}/organizations/invite/${orgId}`,
      method: 'post',
      headers: { 'x-access-token': token },
      data:{
        email:email
      },
      success: (ret) => {
        if(ret['success'] === true){
          openNotificationWithIcon('success', 'User Invited', '')
          this.setState({
            inviteActive:false
          });
        }
      },
      error: (err) => {
        openNotificationWithIcon('error', '', err)
      }
    })
  }
  onChange(changedFields){
    this.setState({
      email:changedFields.email
    })
  }
  render() {
    const data = this.props.items;
    const dataSource = data.map((item) => {
      const obj = {};
      Object.keys(item).forEach((key) => {
        obj[key] = key === 'key' ? item[key] : item[key] !== null ? item[key].value : '';
      });
      return obj;
    });
    const columns = this.columns;
    return (<div>
      {this.state.inviteActive && <InviteForm onChange={this.onChange.bind(this)} onSubmit={this.submitInvite.bind(this)}/>}
      <Button className="editable-add-btn" type="ghost" onClick={this.handleAdd.bind(this)}>Add</Button>
      <Table bordered dataSource={dataSource} columns={columns}/>
    </div>);
  }
}

const InviteForm = Form.create({
    onFieldsChange(props, changedFields) {
      props.onChange(changedFields);
    },
    onSubmit(){
      console.log('submit');
      this.props.submitInvite()
    }
  })(React.createClass({
  handleSubmit(e){
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit();
      }
    });
  },
  render(){
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 16 },
    };
    return(
      <div className="invite">
        <div className="table">
          <div className="cell align-center">
            <div className="invite-inner">
              <div>Please enter the email address:</div><br/>
              <Form inline onSubmit={this.handleSubmit}>
                <FormItem {...formItemLayout} label="E-mail" hasFeedback>
                  {getFieldDecorator('email', {
                    rules: [{
                      type: 'email', message: 'The input is not valid E-mail!',
                    }, {
                      required: true, message: 'Please input your E-mail!',
                    }],
                  })(
                    <Input />
                  )}
                </FormItem>
                <FormItem>
                  <Button type="primary" htmlType="submit">Invite</Button>
                </FormItem>
              </Form>
            </div>
          </div>
        </div>
      </div>
    )
  },
  componentDidMount(){
    setTimeout(function(){
      document.querySelector('.invite').classList.add('active');
    },100);
  }
}));
