import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, hashHistory } from 'react-router'
// Views
import App from './views/App.jsx'
import Login from './views/Login.jsx'
import Dashboard from './views/Dashboard.jsx'
import SiteGroupIndex from './views/SiteGroupIndex.jsx'
import SiteGroup from './views/SiteGroup.jsx'
import FormIndex from './views/FormIndex.jsx'
import CreateForm from './views/CreateForm.jsx'
import DynamicForm from './views/DynamicForm.jsx'
import OrganizationIndex from './views/OrganizationIndex.jsx'
import MapView from './views/MapView.jsx'
import enUS from 'antd/lib/locale-provider/en_US'
import { LocaleProvider } from 'antd'
// css
import style from './styles.scss'
import $ from 'jquery'
import config from '../config'

const replacePathVars = (pathname, state) => {
  return {
    pathname: pathname,
    state: {nextPathname: state}
  }
}

const checkAuth = (nextState, replace) => {
  const token = localStorage.getItem('mappingJwtToken')

  if (token) {
    $.ajax({
      url: `${config.apiUrl}/auth/verify`,
      method: 'post',
      data: {token: token},
      success: ((res) => {
        if (!res.email) {
          replace(replacePathVars('/login', nextState.location.pathname))
        }
      }),
      error: ((err) => {
        replace(replacePathVars('/login', nextState.location.pathname))
      })
    })
  } else {
    replace(replacePathVars('/login', nextState.location.pathname))
  }
}

const AppRoutes = (
  <LocaleProvider locale={enUS}>
    <Router history={hashHistory}>
      <Route path='/' component={App}>
        <Route path='login' component={Login} />
        <Route path='dashboard' component={Dashboard} onEnter={checkAuth} />
        <Route path='sitegroups' component={SiteGroupIndex} onEnter={checkAuth} />
        <Route path='sitegroups/:siteGroupid' component={SiteGroup} onEnter={checkAuth} >
          <Route path='mapview' component={MapView} onEnter={checkAuth} />
        </Route>
        <Route path='forms' component={FormIndex} />
        <Route path='forms/new' component={CreateForm} />
        <Route path='forms/:id' component={DynamicForm} />
        <Route path='organizations' component={OrganizationIndex} />
      </Route>
    </Router>
  </LocaleProvider>
)

ReactDOM.render(
  AppRoutes,
  document.getElementById('render')
)
