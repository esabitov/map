var devConfig = require('./development')

let config = {
  'apiUrl': 'http://localhost:3000',
  databaseUrl: 'postgres://elliotsabitov:@localhost:5432/map',
  databaseOptions: {
    dialect: 'postgres',
    logging: false,
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    }
  }
}

if (process.env.NODE_ENV === 'development') {
  config = Object.assign({}, config, devConfig)
}

module.exports = config
