let config = {
  databaseUrl: 'postgres://localhost:5432/map',
  databaseOptions: {
    dialect: 'postgres',
    logging: false,
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    }
  }
}

module.exports = config
