const Sequelize = require('sequelize')
const db = require('../db.js')

const Site = db.define('Site', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  sitegroup_id: Sequelize.INTEGER,
  street_number: Sequelize.STRING,
  street: Sequelize.STRING,
  unit: Sequelize.STRING,
  city: Sequelize.STRING,
  state: Sequelize.ENUM('AK', 'AL', 'AR', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'HI', 'IA', 'ID', 'IL', 'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO', 'MS', 'MT', 'NC', 'ND', 'NE', 'NH', 'NJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VA', 'VT', 'WA', 'WI', 'WV', 'WY'),
  zip: Sequelize.INTEGER,
  extended_zip: Sequelize.INTEGER,
  parcel: Sequelize.STRING,
  lat: Sequelize.DECIMAL(10, 8),
  lng: Sequelize.DECIMAL(10, 8),
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE
  }
})

module.exports = Site
