const Sequelize = require('sequelize')
const { DataTypes } = Sequelize
const db = require('../db.js')

const DynamicForm = db.define('DynamicForm', {
  title: DataTypes.STRING,
  user_id: DataTypes.INTEGER,
  organization_id: DataTypes.INTEGER
}, {
  classMethods: {
    associate: (models) => {
      // associations can be defined here
    }
  }
})

module.exports = DynamicForm
