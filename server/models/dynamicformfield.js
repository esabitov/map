const Sequelize = require('sequelize')
const { DataTypes } = Sequelize
const db = require('../db.js')

const DynamicFormField = db.define('DynamicFormField', {
  type: DataTypes.STRING,
  form_id: DataTypes.INTEGER,
  required: DataTypes.BOOLEAN,
  value: DataTypes.STRING,
  name: DataTypes.STRING,
  dynamic_form_id: {
    type: Sequelize.INTEGER,

    references: {
      model: 'DynamicForms',
      key: 'id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
  },
}, {
  classMethods: {
    associate: (models) => {
      // associations can be defined here
    }
  }
})

module.exports = DynamicFormField
