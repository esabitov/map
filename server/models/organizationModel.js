const Sequelize = require('sequelize')
const db = require('../db.js')

const OrganizationModel = db.define('Organization', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE
  }
}, {
  classMethods: {
    associate: function (models) {
        // associations can be defined here
    }
  }
})

module.exports = OrganizationModel
