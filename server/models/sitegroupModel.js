const Sequelize = require('sequelize')
const db = require('../db.js')

const SiteGroup = db.define('SiteGroup', {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: Sequelize.INTEGER
  },
  title: {
    type: Sequelize.STRING
  },
  organization_id: {
    type: Sequelize.INTEGER,

    references: {
      model: 'Organizations',
      key: 'id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }
  },

  user_id: {
    type: Sequelize.INTEGER,

    references: {
      model: 'Users',
      key: 'id',
      deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
    }

  },
  createdAt: {
    allowNull: false,
    type: Sequelize.DATE
  },
  updatedAt: {
    allowNull: false,
    type: Sequelize.DATE
  }
})

module.exports = SiteGroup
