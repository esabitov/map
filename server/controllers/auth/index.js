const express = require('express')
const router = express.Router()
const authController = require('./authController')
// const controller = require('./authController')

router.post('/login', (req, res, next) => {
  authController.login(req, res, next)
})
router.post('/verify', (req, res, next) => {
  authController.verify(req, res, next)
})

module.exports = router
