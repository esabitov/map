// const Promise = require('bluebird')
const Organization = require('../../models/organizationModel')
const User = require('../../models/userModel')

const notAdminResponse = { success: false, message: 'must be an admin to create an organization' }
const NoOrganizationResponse = { success: false, message: 'Cannot find organization with that id' }

exports.params = (req, res, next, id) => {
  console.log('here')
  Organization.findById(id)
    .then((org) => {
      if (org === undefined) { res.send(NoOrganizationResponse) }
      req.org = org
      next()
      return null
    })
    .catch((err) => { next(err) })
}

// exports.post = (req, res, next) => {
//   if (!req.decoded.scope === 'admin') res.send(notAdminResponse)

//   async function getOrgAndSaveUser () {
//     let createOrg = Organization.create({ name: req.body.name })
//     let findUser = User.findById(req.decoded.id)

//     let org = await createOrg
//     let user = await findUser

//     user.organization_id = org.id
//     user.save()
//       .then((savedUser) => { res.send(org) })
//       .catch((err) => { res.send(err) })
//   }
//   getOrgAndSaveUser()
// }

exports.post = (req, res, next) => {
  Organization.create(req.body)
    .then((org) => res.send(org))
    .catch((err) => next(err))
}

exports.put = (req, res, next) => {
  if (!req.decoded.scope === 'admin') res.send(notAdminResponse)
  req.org.name = req.body.name
  req.org.save()
    .then((org) => { res.send(org) })
    .catch((err) => { res.send(err) })
}

exports.getOne = (req, res, next) => {
  res.send(req.org)
}

exports.get = (req, res, next) => {
  Organization.findAll()
    .then((orgs) => { res.send(orgs) })
    .catch((err) => { res.send(err) })
}

exports.delete = (req, res, next) => {
  Organization.destroy({
    where: {
      id: req.params.id
    }
  })
  .then((destroyedOrg) => {
    res.send(req.params.id)
  })
  .catch((err) => {
    res.send(err)
  })
}

exports.inviteUser = (req, res, next) => {
  var path           = require('path')
  var templateDir   = path.join(process.env.PWD, 'templates', 'organizationInvite')
  var EmailTemplate = require('email-templates').EmailTemplate
  var template = new EmailTemplate(templateDir)
  var users = [
    {
      data: {
        orgId: req.params.id,
        email: req.body.email
      }
    }
  ]

  var templates = users.map(function (user) {
    return template.render(user)
  })

  //setup nodemailer
  var nodemailer = require('nodemailer');
  // create reusable transporter object using the default SMTP transport
  //var transporter = nodemailer.createTransport('smtps://elliotsabitov@gmail.com:NEED_TO_PUT_PASSWORD_HERE@smtp.gmail.com');
  var transporter = nodemailer.createTransport({
    service: 'Mailgun',
    auth: {
     user: 'postmaster@maps.webdevelocity.com',
     pass: '595e1a032f7a99e0fed0b23b73799c79'   
    }
  });

  Promise.all(templates)
    .then((results)=>{
      console.log(results[0].html)
      console.log(results[0].text)
      console.log(results[0].subject)
      // setup e-mail data with unicode symbols
      var mailOptions = {
          from: '"elliot sabitov ?" <elliotsabitov@gmail.com>', // sender address
          to: 'elliotsabitov@gmail.com, Tomyancey1@gmail.com', // list of receivers
          subject: results[0].subject, // Subject line
          text: results[0].text, // plaintext body
          html: results[0].html // html body
      };
      // send mail with defined transport object
      transporter.sendMail(mailOptions, function(error, info){
        if(error){
          return console.log(error);
        }
        console.log('Message sent: ' + info.response);
      });
    },(error)=>{
      console.error(error); 
    })
    .then((results)=>{
      return res.send({success:true})
    })
}
