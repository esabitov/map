const express = require('express')
const router = express.Router()
const organizationController = require('./organizationController')

router.param('id', organizationController.params)

router.route('/')
  .get(organizationController.get)
  .post(organizationController.post)

router.route('/:id')
  .get(organizationController.getOne)
  .put(organizationController.put)
  .delete(organizationController.delete)

router.route('/invite/:id')
  .post(organizationController.inviteUser)

module.exports = router
