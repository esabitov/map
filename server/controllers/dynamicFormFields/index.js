const express = require('express')
const router = express.Router({mergeParams: true})
const DynamicFormFieldController = require('./DynamicFormFieldController')

// router.param('id', DynamicFormFieldController.params)

router.route('/')
  .get(DynamicFormFieldController.get)
  // .post(DynamicFormFieldController.post)

router.route('/:id')
  // .get(siteController.getOne)
  // .put(sitegroupController.put)
  // .delete(sitegroupController.delete)

module.exports = router
