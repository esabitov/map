const DynamicFormField = require('../../models/dynamicformfield')

exports.get = (req, res, next) => {
  DynamicFormField.findAll({
    where: {
      dynamic_form_id: req.params.formId
    }
  })
    .then((formFields) => {
      console.log(`formFields success -- ${formFields}`)
      res.send(formFields)
    })
    .catch((err) => {
      console.log(err)
      next(err)
    })
}
