const Site = require('../../models/siteModel')
const $ = require('jquery')
const request = require('request')

exports.params = (req, res, next, id) => {
  Site.findById(id)
    .then((site) => {
      req.site = site
      next()
    })
    .catch((err) => next(err))
}

exports.get = (req, res, next) => {
  Site.findAll({
    where: {
      sitegroup_id: req.params.siteGroupId
    }
  })
    .then((sites) => res.send(sites))
    .catch((err) => next(err))
}

exports.put = (req, res, next) => {
  Site.update(req.body, {
    fields: [ 'street_number', 'street', 'unit', 'city', 'state', 'zip', 'extended_zip', 'parcel', 'lat', 'lng' ],
    where: {id: req.params.id},
    returning: true, // need to this to get result[1].dataValues
    plain: true // need to this to get result[1].dataValues
  })
  .then((result) => {
    res.send(result[1].dataValues)
  })
  .catch((err) => next(err))
}

exports.post = (req, res, next) => {
  console.log('posting a site')
  let siteParams = Object.assign({}, req.body, {sitegroup_id: req.params.siteGroupId})
  Site.create(siteParams)
    .then((site) => res.send(site))
    .catch((err) => next(err))
}

exports.delete = (req, res, next) => {
  Site.destroy({
    where: {
      id: req.params.id
    }
  })
  .then((destroyedSite) => {
    res.send(req.params.id)
  })
  .catch((err) => {
    res.send(err)
  })
}

exports.geocode = (req, res, next) => {
  emptyForNull = (thing) => {
    console.log(thing)
    if (thing === null || thing === undefined) return ''
    return thing
  }

  parseGoogleDataAndSave = (data) => {
    let locationData = JSON.parse(data)
    if (locationData.status === 'OK' && locationData.results.length > 0){
      let loc = locationData.results[0]
      let { formatted_address, geometry, place_id, partial_match } = loc
      let { location_type, viewport, location } = geometry
      let { lat, lng } = location
      console.log('here')
      if (lat && lng){
        req.site.update({lat, lng}, {
          fields: [ 'lat', 'lng' ],
          where: {id: req.params.id},
          returning: true, // need to this to get result[1].dataValues
          plain: true // need to this to get result[1].dataValues
        })
        .then((result) => {
          console.log(result)
          res.sendJson(result)
        })
        .catch((err) => next(err))
      }
      res.sendJson({success: false, err})
    }
  }

  geocodeFromGoogle = (site) => {
    return new Promise((resolve, reject) => {
      let { street_number, street, unit, city, state, zip } = site
      let addressString = `${emptyForNull(street_number)} ${emptyForNull(street)} ${emptyForNull(unit)} ${emptyForNull(city)} ${emptyForNull(state)} ${emptyForNull(zip)}`
      let address = addressString.trim()
      request({
        uri: 'https://maps.googleapis.com/maps/api/geocode/json',
        qs: { address: address }
      }, (err, requestRes, body ) => {
        if (err) {
          return reject(err)
        } else if (res.statusCode !== 200){
          err = new Error('unexpected status code' + res.statusCode)
          return reject(err)
        }
        return resolve(body)
      })
    })
  }

  geocodeFromGoogle(req.site).then((body) => {
    parseGoogleDataAndSave(body)
  })
  .catch((err) => {res.send(err)})
}
