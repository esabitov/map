const express = require('express')
const users = require('./user')
const sitegroups = require('./sitegroups')
const sites = require('./sites')
const auth = require('./auth')
const organizations = require('./organizations')
const dynamicForms = require('./dynamicForms')
const dynamicFormFields = require('./dynamicFormFields')
const parcels = require('./parcels')
const authMiddleware = require('../middleware/authMiddleware')
const unlessSkip = require('../middleware/unlessSkip')

const router = express.Router()
const whitelistedRoutes = {
  '/users': 'POST',
  '/auth/login': 'POST',
  '/auth/verify': 'POST'
  // '/posts': 'GET',
  // '/posts/:id': 'GET'
}

router.use(unlessSkip(whitelistedRoutes, authMiddleware))

router.use('/auth', auth)
router.use('/users', users)
router.use('/sitegroups', sitegroups)
router.use('/sitegroups/:siteGroupId/sites', sites)
router.use('/organizations', organizations)
router.use('/forms', dynamicForms)
router.use('/forms/:formId/formFields', dynamicFormFields)
router.use('/parcels', parcels)
// router.use('/categories', categories)

module.exports = router
