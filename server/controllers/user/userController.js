const User = require('../../models/userModel')
const Auth = require('../../models/authModel')
const bcrypt = require('bcrypt')

exports.params = function(req, res, next, id) {
  User.findById(id)
    .then(function(user) {
      if (!user) {
        next(new Error('No user with that id'))
      } else {
        req.user = user
        next()
      }
    }, function (err) {
      next(err)
    })
}

exports.get = (req, res, next) => {
  User.findAll({
    order: [ ['createdAt', 'ASC'] ]
  }).then((users) => {
    res.send(users)
  }).catch((err) => {
    res.send(err)
  })
}

exports.getOne = (req, res, next) => {
  res.send(user)
}

exports.put = (req, res, next) => {
  // hijack to add admin for now
  req.user.admin = true
  console.log(req.user.save)
  req.user.save()
    .then((user) => { res.json(user) })
    .catch((err) => { res.json(err) })
}

exports.create = (req, res, next) => {
  let auth = new Auth()
  console.log(req.body)
  auth.createPassword(req.body.password).then((hashedPassword) => {
    // temporary add organization id for now
    let userParams = Object.assign({}, req.body, {password: hashedPassword, organization_id: 1})
    User.create(userParams).then((user) => {
      bcrypt.compare(req.body.password, user.password).then((authenticated) => {
        auth.user = user
        auth.authenticated = authenticated
        let token = auth.createTokenOrDenyAccess(req)
        res.send(token)
      })
    }).catch((err) => {
      next(err)
    })
  })
  .catch((err) => {
    next(err)
  })
}

exports.delete = (req, res, next) => {
  User.destroy({
    where: {
      id: req.user.id
    }
  })
  .then((destroyedUser) => {
    res.send(req.user)
  })
  .catch((err) => {
    res.send(err)
  })
}
