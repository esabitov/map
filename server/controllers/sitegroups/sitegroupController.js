const fs = require('fs')
const csv = require('fast-csv')
const SiteGroup = require('../../models/sitegroupModel')
const Site = require('../../models/siteModel')
const NoSiteGroupFound = { success: false, message: 'No sitegroup found with that id' }

exports.params = (req, res, next, id) => {
  console.log(`${id} -- id here `)
  SiteGroup.findById(id)
    .then((siteGroup) => {
      if (!siteGroup) { res.send(NoSiteGroupFound) }
      req.siteGroup = siteGroup
      next()
    })
    .catch((err) => { res.send(err) })
}

exports.get = (req, res, next) => {
  SiteGroup.findAll({
    where: {
      organization_id: req.decoded.organization_id
    }
  })
  .then((siteGroups) => { res.send(siteGroups) })
  .catch((err) => { res.send(err)} )
}

exports.getOne = (req, res, next) => {
  res.send(req.siteGroup)
}

exports.put = (req, res, next) => {
}

exports.post = (req, res, next) => {
  console.log(`scope --- ${req.decoded.scope}`)
  if (req.decoded.scope !== 'admin') { res.send({success: false, message: 'must be a user admin to create a site group'}) }
  let { id, organization_id } = req.decoded
  let siteGroupParams = Object.assign({}, req.body, { user_id: id, organization_id })
  SiteGroup.create(siteGroupParams)
    .then((siteGroup) => {
      siteGroup = Object.assign({}, siteGroup.toJSON(), {success: true})
      res.send(siteGroup)
    })
    .catch((err) => { res.send(err) })
}

exports.download = (req, res, next) => {
  var exportFileName = `${req.siteGroup.title}-${Date.now()}.csv`
  let writeData = (sites) => {
    let csvStream = csv.createWriteStream({headers: true})
    let writableStream = fs.createWriteStream(`downloads/${exportFileName}`)
    res.setHeader('Content-disposition', `attachment; filename=${exportFileName}`);
    res.setHeader('Content-type', 'application/CSV');
    writableStream.on('finish', function () { res.download(`downloads/${exportFileName}`) })
    csvStream.pipe(writableStream)
    sites.forEach((site, i) => {
      csvStream.write(site.dataValues)
    })
    csvStream.end()
  }

  Site.findAll({ where: { sitegroup_id: req.siteGroup.id } })
    .then((sites) => writeData(sites))
}

exports.delete = (req, res, next) => {}
