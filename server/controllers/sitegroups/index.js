const express = require('express')
const router = express.Router()
const sitegroupController = require('./sitegroupController')
const fs = require('fs')
const multer = require('multer')
const upload = multer({dest: './uploads'})
const csv = require('fast-csv')
const SiteGroup = require('../../models/sitegroupModel')
const Site = require('../../models/siteModel')

router.param('id', sitegroupController.params)

router.route('/')
  .post(sitegroupController.post)
  .get(sitegroupController.get)

router.route('/:id')
  .get(sitegroupController.getOne)
  // .put(sitegroupController.put)
  // .delete(sitegroupController.delete)
router.route('/:id/download')
  .post(sitegroupController.download)
router.route('/:id/upload')
  .post(upload.single('file'), ((req, res, next) => {
    let processingData = []
    let processingCSV = true
    let countOfFiles = 0
    var stream = fs.createReadStream('./' + req.file.path)
    var csvStream = csv({headers: true, trim: true})
      .on('data', (data) => {
        console.log(data)
        processingData.push(data)
      })

      .on('end', () => {
        processingCSV = false
        const assignableParts = {
          user_id: req.decoded.id,
          organization_id: req.decoded.organization_id,
          sitegroup_id: Number(req._parsedUrl.path.match(/(\d+)/)[0])
        }
        const siteGroupCreator = processingData.map((siteGroup) => {
          return Site.create(Object.assign(siteGroup, assignableParts))
        })

        Promise.all(siteGroupCreator)
          .then((result) => res.send(result))
          .catch((err) => res.send(err))
      })

    stream.pipe(csvStream)
  })
)

module.exports = router
