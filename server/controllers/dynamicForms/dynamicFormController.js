const DynamicForm = require('../../models/dynamicform')
const DynamicFormField = require('../../models/dynamicformfield')
const notAdminResponse = { success: false, message: 'must be an admin to create an organization' }
const noForm = (id) => { return {success: false, message: `Form not found with id ${id}`} }

exports.params = (req, res, next, id) => {
  console.log(`id ${id}`)
  DynamicForm.findById(id)
    .then((form) => {
      if (form === undefined) { res.send(noForm(id)) }
      console.log(`form --- ${form}`)
      req.form = form
      next()
    })
    .catch((err) => { next(err) })
}

exports.post = (req, res, next) => {
  if (!req.decoded.scope === 'admin') res.send(notAdminResponse)
  let { formTitle, formFields } = req.body
  let { id, organization_id } = req.decoded

  const createFormFields = (form, formFields) => {
    return Promise.all(formFields.map((formField) => {
      return DynamicFormField.create(Object.assign({}, formField, { dynamic_form_id: form.id }))
    }))
  }

  async function createFormAndFormFields () {
    let createForm = DynamicForm.create({ title: formTitle, user_id: id, organization_id })
    let form = await createForm
    let createdFormFields = await createFormFields(form, formFields)
    res.send(createdFormFields)
  }

  createFormAndFormFields()
}

exports.get = (req, res, next) => {
  DynamicForm.findAll()
    .then((dynamicForms) => { res.send(dynamicForms) })
    .catch((err) => { res.send(err) })
}

exports.getOne = (req, res, next) => {
  res.send(req.form)
}
