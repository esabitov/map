const express = require('express')
const router = express.Router()
const dynamicFormController = require('./dynamicFormController')

router.param('id', dynamicFormController.params)

router.route('/')
  .get(dynamicFormController.get)
  .post(dynamicFormController.post)

router.route('/:id')
  .get(dynamicFormController.getOne)
  // .put(sitegroupController.put)
  // .delete(sitegroupController.delete)

module.exports = router
