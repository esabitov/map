const Parcel = require('../../models/parcel')
const notAdminResponse = { success: false, message: 'must be an admin to create an organization' }
const noForm = (id) => { return {success: false, message: `Form not found with id ${id}`} }

exports.get = (req, res, next) => {
  buildFeature = (data) => {
    // newData = Object.assign({}, data, {density: 1100})
    // console.log(newData)
    return {'type':'Feature','geometry': data.Shape, id: data.id.toString(), 'properties':{name:data.id, density:1100} }
  }
  Parcel.findAll({limit: 10})
    .then((parcels) => {
      resp = parcels.map((parcel) => buildFeature(parcel))
      var outerJson = { "type": "FeatureCollection", "features": resp }
      res.send(outerJson)
    })
    .catch((err) => res.send(err))
}
