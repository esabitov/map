const express = require('express')
const router = express.Router()
const parcelController = require('./parcelController')

router.route('/')
  .get(parcelController.get)
  // .post(parcelController.post)

// router.route('/:id')
//   .get(parcelController.getOne)
  // .put(sitegroupController.put)
  // .delete(sitegroupController.delete)

module.exports = router
