// const mongooes = require('mongoose')
// const Promise = require('bluebird')
// const User = require('../api/user/userModel')
// const Category = require('../api/category/categoryModel')
// const Post = require('../api/post/postModel')

// let users = [
//   {
//     username: 'james',
//     password: 'testing123'
//   },
//   {
//     username: 'steve',
//     password: 'testing123'
//   },
//   {
//     username: 'Ricardo',
//     password: 'testing123'
//   },
// ]

// let seedCategories = [
//   {
//     name: 'cats'
//   },
//   {
//     name: 'pizza'
//   },
//   {
//     name: 'pasta'
//   }
// ]

// let posts = [
//   {
//     title: 'not working',
//     text: 'this post will not work on generation'
//   },
//   {
//     title: 'another',
//     text: 'since the above wont work this wont either'
//   },
//   {
//     title: 'go again',
//     text: 'been sticking to the threes rule'
//   },
//   {
//     title: 'for posterity',
//     text: 'this post will not work on generation'
//   },
//   {
//     title: 'resilience',
//     text: 'since the above wont work this wont either'
//   },
//   {
//     title: 'yoga',
//     text: 'been sticking to the threes rule'
//   }
// ]

// function dropUsers() {
//   return new Promise((resolve, reject) => {
//     User.remove({},(err, removed) => {
//       if (err){
//         reject(console.log('error'))
//       } else {
//         resolve('users dropped')
//       }
//     })
//   })
// }

// function dropCategories() {
//   return new Promise((resolve, reject) => {
//     Category.remove({}, (err, removed) => {
//       if (err){
//         console.log('rejected')
//         reject(err)
//       } else {
//         resolve('categories dropped')
//       }
//     })
//   })
// }

// function createAllUsers() {
//   let userPromises = users.map(createUser)
//   return Promise.all(userPromises)
//   .then((users) => {
//     return users
//   })
// }

// function createUser(user) {
//   return new Promise((resolve, reject) => {
//     User.create((user), (err, newUser) => {
//       if (newUser){
//         resolve(newUser)
//       } else {
//         reject(err)
//       }
//     })
//   })
// }

// function createAllCategories() {
//   let allPromises = seedCategories.map(createCategory)
//   return Promise.all(allPromises)
//   .then((values) => {
//     return values
//   })
// }

// function createCategory(category) {
//   return new Promise((resolve, reject) => {
//     Category.create((category), (err, cat) => {
//       if (cat){
//         resolve(cat)
//       } else {
//         reject(err)
//       }
//     })
//   })
// }

// function createPost(post, userId) {
//   return new Promise((resolve, reject) => {
//     Object.assign(post, {'author': userId })
//     Post.create((post), (err, post) => {
//       if (err) reject(new Error(err))
//       resolve(post)
//     })
//   })
// }

// function dropAllPosts(){
//   return new Promise((resolve, reject) => {
//     Post.remove({}, (err, removed) => {
//       if (err) reject(err)
//       resolve(removed)
//     })
//   })
// }

// function createAllPostPromises(users) {

//   let promises = []

//   users.forEach((user, i) => {
//     promises.push(createPost(posts[i], user))
//     promises.push(createPost(posts[i + 3], user))
//   })

//   return Promise.all(promises)
//   .then((posts) => {
//     return posts
//   })
// }

// output = (text) => {
//   console.log(text)
// }

// let genBoy = Promise.coroutine(function*(){

//   let dbUserDrop = dropUsers()
//   let dbCatDrop = dropCategories()
//   let dbPostDrop = dropAllPosts()

//   output(yield dbUserDrop)
//   output(yield dbCatDrop)
//   yield dbPostDrop

//   let createCat = createAllCategories()
//   let createUsers = createAllUsers()
//   let newUsers;

//   yield createCat
//   yield createUsers
//   .then((users) => {
//     // console.log(newUsers)
//     newUsers = users
//   })
//   yield createAllPostPromises(newUsers)
//   // yield createAllPosts

// })

// genBoy()
