var config = {
  env: process.env.NODE_ENV || 'development',
  logging: false,

  dev: 'development',
  test: 'testing',
  prod: 'production',
  secrets: {
    secret: 'testing',
    salt: 'salt-testing'
  },
  expireTime: 60 * 60 * 24 * 10,
  port: process.env.PORT || 3000
}

process.env.NODE_ENV = process.env.NODE_ENV || config.env
config.env = process.env.NODE_ENV

var envConfig = require(`${__dirname}/${config.env}`)
module.exports = Object.assign({}, config, envConfig || {})
