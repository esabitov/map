const Sequelize = require('sequelize');
const config = require('../config');
console.log(config)
//Create a shared instance to be used accross models
let db = new Sequelize(config.databaseUrl, config.databaseOptions);

module.exports = db;
