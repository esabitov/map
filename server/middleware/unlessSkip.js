const unlessSkip = function (pathsObject, middleware) {
  return (req, res, next) => {
    let goNext = false
    Object.keys(pathsObject).forEach((pathKey) => {
      console.log(pathKey)
      if (pathKey === req.path && pathsObject[pathKey] === req.method) goNext = true
    })
    if (goNext) {
      return next()
    } else {
      return middleware(req, res, next)
    }
  }
}

module.exports = unlessSkip
