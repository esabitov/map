const jwt = require('jsonwebtoken')
const router = require('express').Router()

router.use((req, res, next) => {
  let token = req.body.token || req.query.token || req.headers['x-access-token']
  if (token) {
    jwt.verify(token, req.app.get('jwtSecret'), (err, decoded) => {
      if (err) {
        return res.json({ success: false, message: 'failed to authenticate access token'})
      } else {
        req.decoded = decoded
        next()
      }
    })
  } else {
    console.log('!token')
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    })
  }
})

module.exports = router
