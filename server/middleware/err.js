var logger = require('../util/logger')

module.exports = (err, req, res, next) => {
  if (err.name == 'ValidationError') {
    logger.log(JSON.stringify(err, {}, 2))
  } else if (err) {
    logger.log(err.stack)
  }
  res.json({success: false, message: err.message})
}
