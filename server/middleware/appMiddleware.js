const bodyParser = require('body-parser')
const morgan = require('morgan')
const express = require('express')

module.exports = function (app) {
  app.use(express.static('public'))

  app.use(bodyParser.urlencoded({extended: true}))
  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token')
    res.header('Access-Control-Allow-Methods', 'PUT, DELETE')

    if ('OPTIONS' == req.method) {
      res.send(200);
    } else {
      next();
    }
  })
  app.use(bodyParser.json())
  app.use(morgan('dev'))
}
