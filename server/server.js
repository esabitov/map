const express = require('express')
const app = express()
//include controllers
const api = require('./controllers')
const err = require('./middleware/err')
const config = require('./config/config.js')

app.set('jwtSecret', config.secrets.secret)

if (config.seed) require('./util/seed')
require('./middleware/appMiddleware')(app)

app.use('/', api)
app.use(err)

module.exports = app
