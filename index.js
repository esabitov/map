var config = require(__dirname + '/server/config/config')
var app = require(__dirname + '/server/server.js')
var logger = require(__dirname + '/server/util/logger')

app.listen(config.port)
// logger.log(`ENV ${process.env.NODE_ENV}`)
logger.log(`listening on ${config.port}`)
